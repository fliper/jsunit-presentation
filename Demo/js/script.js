var exampleNamespace = function () {
	function squareNumber(num) {
		return num*num;
	}
	return {
		squareNumber: squareNumber
	}
}();